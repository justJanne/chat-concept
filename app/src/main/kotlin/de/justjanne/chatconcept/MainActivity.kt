package de.justjanne.chatconcept

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import de.justjanne.chatconcept.theme.ChatConceptTheme
import kotlinx.datetime.Instant

data class User(
  val id: String,
  val displayName: String,
  val avatarUrl: String,
)

sealed class FileAttachment {
  data class Image(val url: String) : FileAttachment()
  data class Video(val url: String) : FileAttachment()
  data class Audio(val url: String) : FileAttachment()
  data class Generic(val url: String) : FileAttachment()
}

sealed class Message {
  data class TextMessage(val timestamp: Instant, val sender: User, val content: String) : Message()
  data class Join(val timestamp: Instant, val sender: User) : Message()
  data class Leave(val timestamp: Instant, val sender: User) : Message()
  data class Kick(val timestamp: Instant, val sender: User, val subject: User) : Message()
  data class AttachmentMessage(
    val timestamp: Instant,
    val sender: User,
    val content: FileAttachment
  ) : Message()
}

data class RoomMember(
  val user: User,
  val powerLevel: Int,
  val membership: Membership
)

enum class Membership {
  Joined,
  Invited,
  Banned
}

sealed class Room {
  data class GroupRoom(
    val displayName: String,
    val topic: String,
    val avatarUrl: String,
    val members: List<RoomMember>,
    val timeline: Timeline,
  ) : Room()

  data class DmRoom(
    val user: User,
    val members: List<RoomMember>,
    val timeline: Timeline,
  ) : Room()
}

data class Timeline(
  val messages: List<Message>,
  val outbox: List<Message>,
)

object SampleClient {
  private val evelyn = User(
    id = "@evelyn:eve.li",
    displayName = "Evelyn",
    avatarUrl = "https://avatars.githubusercontent.com/u/36997087?v=4"
  )

  private val janne = User(
    id = "@justjanne:decentralised.chat",
    displayName = "justJanne",
    avatarUrl = "https://avatars.githubusercontent.com/u/3933349?v=4"
  )

  val rooms: List<Room> = listOf(
    Room.DmRoom(
      user = evelyn,
      members = listOf(
        RoomMember(evelyn, 100, Membership.Joined),
        RoomMember(janne, 100, Membership.Joined),
      ),
      timeline = Timeline(
        messages = listOf(
          Message.TextMessage(
            timestamp = Instant.parse("2023-11-16T17:06:54Z"),
            sender = evelyn,
            content = "meow!"
          ),
          Message.TextMessage(
            timestamp = Instant.parse("2023-11-16T17:07:08Z"),
            sender = evelyn,
            content = "lust zu telefonieren? \uD83E\uDD7A"
          ),
          Message.TextMessage(
            timestamp = Instant.parse("2023-11-16T17:07:12Z"),
            sender = janne,
            content = "miaaaauuu"
          ),
          Message.TextMessage(
            timestamp = Instant.parse("2023-11-16T17:07:22Z"),
            sender = evelyn,
            content = "mag ui basteln \uD83E\uDD7A"
          ),
          Message.TextMessage(
            timestamp = Instant.parse("2023-11-16T17:07:32Z"),
            sender = janne,
            content = "gerne, wie ist's in 2min?"
          ),
          Message.TextMessage(
            timestamp = Instant.parse("2023-11-16T17:07:41Z"),
            sender = evelyn,
            content = "ja"
          ),
        ),
        outbox = emptyList()
      )
    )
  )
}

class SampleMessageProvider : PreviewParameterProvider<Message> {
  override val values = (SampleClient.rooms[0] as Room.DmRoom).timeline.messages.asSequence()
}

@Composable
@Preview(widthDp = 120, showBackground = true)
fun MessageView(
  @PreviewParameter(SampleMessageProvider::class)
  message: Message
) {
  Row {

    Text("Hallo")
  }
}

@Composable
fun Avatar(name: String, imageUrl: String, modifier: Modifier = Modifier) {
  val avatarPainter = rememberAsyncImagePainter(imageUrl)

  Box(modifier) {
    Text(
      name.first().toString(),
      modifier = Modifier.align(Alignment.Center)
    )
    Image(
      avatarPainter,
      contentDescription = null,
      contentScale = ContentScale.Fit,
      modifier = Modifier
        .aspectRatio(1.0f)
        .clip(CircleShape)
        .background(MaterialTheme.colorScheme.primaryContainer)
    )
  }
}

@Composable
@Preview(showSystemUi = true)
fun ChatConceptApp() {
  val room = SampleClient.rooms[0] as Room.DmRoom

  Scaffold(
    topBar = {
      TopAppBar(
        title = { Text(room.user.displayName) },
        navigationIcon = {
          Avatar(
            room.user.displayName,
            room.user.avatarUrl,
            modifier = Modifier.size(48.dp)
          )
        },
        modifier = Modifier.shadow(8.dp)
      )
    }
  ) { padding ->
    Box {
      Column {
        LazyColumn(
          contentPadding = padding,
          modifier = Modifier
            .background(Color.Blue)
            .fillMaxSize()
        ) {
          items(room.timeline.messages) { message ->
            MessageView(message)
          }
        }
        Spacer(Modifier.height(64.dp))
      }
      Surface(
        modifier = Modifier
          .height(240.dp)
          .fillMaxWidth()
          .align(Alignment.BottomCenter)
          .offset(0.dp, 240.dp - 64.dp),
        tonalElevation = 16.dp,
        shadowElevation = 16.dp
      ) {
        Column(Modifier.fillMaxWidth()) {

          Surface(
            color = MaterialTheme.colorScheme.onSurfaceVariant.copy(0.4f),
            shape = MaterialTheme.shapes.extraLarge,
            modifier = Modifier
              .padding(top = 4.dp)
              .align(Alignment.CenterHorizontally)
          ) {
            Box(Modifier.size(width = 32.dp, height = 4.dp))
          }
          InputArea(Modifier.fillMaxSize())
        }
      }
    }
  }
}

class MainActivity : ComponentActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContent {
      ChatConceptTheme {
        ChatConceptApp()
      }
    }
  }
}
