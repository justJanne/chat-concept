package de.justjanne.chatconcept

import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.Send
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
@Preview
fun InputArea(modifier: Modifier = Modifier) {
  var input by remember { mutableStateOf("") }
  val interactionSource = remember { MutableInteractionSource() }

  Row(verticalAlignment = Alignment.CenterVertically, modifier = modifier) {
    BasicTextField(
      value = input,
      onValueChange = { input = it },
      modifier = Modifier
        .weight(1.0f, true)
        .fillMaxHeight(),
      singleLine = false,
      maxLines = 5,
      minLines = 1,
      enabled = true,
      interactionSource = interactionSource,
    ) {
      OutlinedTextFieldDefaults.DecorationBox(
        value = input,
        visualTransformation = VisualTransformation.None,
        innerTextField = it,
        singleLine = false,
        enabled = true,
        placeholder = { Text("Send a meow message…") },
        interactionSource = interactionSource,
        container = {
          OutlinedTextFieldDefaults.ContainerBox(
            enabled = true,
            isError = false,
            colors = OutlinedTextFieldDefaults.colors(
              focusedBorderColor = Color.Transparent,
              unfocusedBorderColor = Color.Transparent,
              errorBorderColor = Color.Transparent,
              disabledBorderColor = Color.Transparent,
            ),
            interactionSource = interactionSource,
            shape = RectangleShape,
            unfocusedBorderThickness = 0.dp,
            focusedBorderThickness = 0.dp
          )
        }
      )
    }
    IconButton(
      onClick = { },
      Modifier
        .padding(4.dp)
        .align(Alignment.Top)
    ) {
      Icon(
        Icons.AutoMirrored.Default.Send,
        contentDescription = null,
        tint = MaterialTheme.colorScheme.secondary
      )
    }
  }
}
